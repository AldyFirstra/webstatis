<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        $namaDepan = $request->input('firstName');
        $namaBlk = $request->input('lastName');

        return view('welcomeGet',[
            'namaDepan' => $namaDepan,
            'namaBlk'   => $namaBlk
        ]);
    }
}
