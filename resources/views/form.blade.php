<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pertemuan 1</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <form action="/welcome" method="POST">
        @csrf
        <h2>Sign Up Form</h2>

        <p>First name:</p>
        <input type="text" name="firstName" id="firstName">

        <p>Last name:</p>
        <input type="text" name="lastName" id="lastName">

        <p>Gender:</p>

        <input type="radio" name="gender" id="gender"> Male <br>
        <input type="radio" name="gender" id="gender"> Female <br>
        <input type="radio" name="gender" id="gender"> Other

        <p>Nationality:</p>
        <select name="nation" id="nation">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
            <option value="4">Australia</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name="lng" id="lng"> Bahasa Indonesia <br>
        <input type="checkbox" name="lng" id="lng"> English <br>
        <input type="checkbox" name="lng" id="lng"> Other

        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>

        <button type="submit">Sign Up</button>
    </form>

</body>

</html>